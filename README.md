**Реализованная функциональность**  
Определение параметров для дерева решений;  
Дерево решений;  
Интерактивная карта;  
Данные можно посмотреть за определённый период времени;  

**Основной стек технологий:**  
HTML, CSS, JavaScript, React (Next.js), Python (Pandas, NumPy, Jupyter).  
Webpack, Babel.  

**Демо**  
Демо сервиса доступно по адресу: https://minergo.vercel.app 

**СРЕДА ЗАПУСКА**  
Развертывание сервиса производится на любой ОС по средством Node.js и пакетного менеджера npm; 

**УСТАНОВКА**  
Установка Node.js https://nodejs.org/ru/  
После установки Node.js нужно выполнить в консоли следующие команды:  
git clone https://gitlab.com/cyberfarsh/minergo.git  
cd ./frontend/minergo_map/  
npm install  
next run dev  

**РАЗРАБОТЧИКИ**  

Рындин Денис _Fullstack_ https://t.me/pustoiden  
Владимиров Александр _Data Analyst_ https://t.me/@garvild  
Юров Ярослав _Backend разработчик_ https://vk.com/jolly_roger123  
