import * as React from 'react';

function formatTime(time) {
  const date = new Date(time);
  return `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`;
}

function ControlPanel(props) {
  const {startTime, endTime, onChangeTime, allDays, onChangeAllDays, selectedTime} = props;
  const day = 24 * 60 * 60 * 1000;
  const days = Math.round((endTime - startTime) / day);
  const selectedDay = Math.round((selectedTime - startTime) / day);

  const onSelectDay = evt => {
    const daysToAdd = evt.target.value;
    const newTime = startTime + daysToAdd * day;
    onChangeTime(newTime);
  };

  return (
    <div className="control-panel">
      <h3>Карта "Минерго"</h3>
      <p>
        Карта показывает возможные ЧП
        <br />
        с <b>{formatTime(startTime)}</b> по <b>{formatTime(endTime)}</b>.
      </p>
      <hr />
      <div className="input">
        <label>Показать за все дни</label>
        <input
          type="checkbox"
          name="allday"
          checked={allDays}
          onChange={evt => onChangeAllDays(evt.target.checked)}
        />
      </div>
      <div className={`input ${allDays ? 'disabled' : ''}`}>
        <label>Выбранный день: {formatTime(selectedTime)}</label>
        <input
          type="range"
          disabled={allDays}
          min={1}
          max={days}
          value={selectedDay}
          step={1}
          onChange={onSelectDay}
        />
      </div>
      <hr />
      <p>
        Источник данных:{' '}
        <a href="https://rp5.ru/">
          rp5.ru
        </a>
      </p>
      <div className="source-link">
        <a
          href="https://gitlab.com/cyberfarsh/minergo"
          target="_new"
        >
          Посмотреть код ↗
        </a>
      </div>
    </div>
  );
}

export default React.memo(ControlPanel);