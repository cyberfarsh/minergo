import * as React from 'react';

function CityInfo(props) {
  const {info} = props;

  const date_time = new Date(info.properties.time)
  var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
  var year = date_time.getFullYear();
  var day = date_time.getDate();
  var month = months[date_time.getMonth()];

  const displayName = `Причина: ${info.reason},`;

  return (
    <div>
      <div>
        {displayName}<br/>
        {`дата: ${day}.${month}.${year}`}
      </div>
    </div>
  );
}

export default React.memo(CityInfo);