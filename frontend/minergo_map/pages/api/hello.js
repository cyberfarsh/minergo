// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import weather from '../../components/Map/weather.json'


export default (req, res) => {
  res.status(200).json(weather)
}
